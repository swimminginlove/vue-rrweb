# vue-rrweb

['vue', 'rrweb'].join('-');

https://www.rrweb.io/

⚠️ WORK IN PROGRESS. NO TESTS AND NOT MANUALLY TESTED EITHER. JUST JAMMING. ⚠️

## usage

```vue
<script>
import { VueRrwebRecorder } from 'vue-rrweb';
export default {
  components: {
    VueRrwebRecorder,
  },
}
</script>

<template>
  <vue-rrweb-recorder
    :tag="div"
    :max-flush-length="100"       <!-- flush events when events array length reaches N -->
    :max-flush-interval="5000"    <!-- flush events every N milliseconds -->
    @event="onEvent"
    @flush="onFlush"
  />
</template>
```

```vue
<script>
import { VueRrwebRecorder } from 'vue-rrweb';
export default {
  components: {
    VueRrwebRecorder,
  },
}
</script>

<template>
  <vue-rrweb-recorder :auto-start-recording="false">
    <template slot-scope={ startRecording, stopRecording }>
      <div>
        <button @click="startRecording"/>
        <button @click="stopRecording"/>
      </div>
    </template>
  </vue-rrweb-recorder>
</template>
```

```vue
<script>
import { VueRrwebRecorder } from 'vue-rrweb';
import MyRecorderController from './MyRecorderController.vue';

export default {
  components: {
    VueRrwebRecorder,
    MyRecorderController,
    MyFeature: {
      inject: ['startRecording', 'stopRecording'],
      mounted() {
        this.startRecording();
      },
      beforeDestroy() {
        this.stopRecording();
      },
    },
  },
};
</script>

<template>
  <vue-rrweb-recorder :auto-start-recording="false">
    <my-feature/>
    <my-recorder-controller/>
  </vue-rrweb-recorder>
</template>
```
