import { record } from 'rrweb';
import { timingSafeEqual } from 'crypto';

export default {
  name: 'vue-rrweb-recorder',

  provide() {
    return this.controlObject;
  },

  props: {
    tag: {
      type: String,
      required: false,
      default: 'div',
    },
    maxFlushLength: {
      type: Number,
      required: false,
      default: -1,
    },
    maxFlushInterval: {
      type: Number,
      required: false,
      default: -1,
    },
  },

  data() {
    return {
      events: [],
      flushIntervalId: -1,
      stopRecordingFunction() {},
    };
  },

  computed: {
    controlObject() {
      const { stopRecording, startRecording } = this;

      return {
        stopRecording,
        startRecording,
      };
    },

    hasEnabledFlushInterval() {
      return this.maxFlushInterval !== -1;
    },

    hasEnabledFlushLength() {
      return this.flushIntervalId !== -1;
    },

    hasEnabledAutoFlush() {
      return this.hasEnabledFlushInterval || this.hasEnabledFlushLength;
    },

    isAtFlushCapacity() {
      return (
        this.hasEnabledFlushLength && this.events.length >= this.maxFlushLength
      );
    },
  },

  methods: {
    startRecording() {
      const { emit } = this;

      this.stopRecordingFunction = record({ emit });

      if (this.hasEnabledFlushInterval) this.startFlushInterval();
    },

    stopRecording() {
      this.stopRecordingFunction();

      if (this.hasEnabledFlushLength) clearTimeout(this.flushIntervalId);
    },

    emit(event) {
      this.$emit('event', event);

      if (this.hasEnabledAutoFlush) this.events.push(event);

      if (this.isAtFlushCapacity) this.flush();
    },

    flush() {
      const events = this.events.splice(0, this.events.length);
      this.$emit('flush', events);
    },

    startFlushInterval() {
      this.flushIntervalId = setInterval(
        () => this.flush(),
        this.maxFlushInterval,
      );
    },
  },

  mounted() {
    this.startRecording();
  },

  render(createElement) {
    const children = this.$scopedSlots.default
      ? this.$scopedSlots.default(this.controlObject)
      : [];
    return createElement(this.tag, children);
  },
};
